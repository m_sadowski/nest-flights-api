import { Module } from '@nestjs/common';
import { FlightsController } from './flights.controller';
import { FlightsService } from './flights.service';
import { flightsProviders } from './flights.providers';
import { DatabaseModule } from '../database/database.module';
import { FlightsResolver } from './flights.resolver';

@Module({
  imports: [DatabaseModule],
  controllers: [FlightsController],
  providers: [FlightsService, ...flightsProviders, FlightsResolver],
  exports: [FlightsResolver],
})
export class FlightsModule {}
