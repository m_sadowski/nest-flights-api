import { Controller, Get, CacheTTL, Query, HttpException } from '@nestjs/common';
import { LiveService } from './live.service';
import { Flight } from './lib/flightradar';

@Controller('live')
export class LiveController {
  constructor(private readonly liveService: LiveService) {}

  @Get()
  @CacheTTL(300)
  async getLiveFlights(
    @Query('callsign') callsign,
    @Query('aircraft') aircraft,
  ): Promise<Flight[]> {
    if (!callsign) {
      throw new HttpException('Incomplete query', 400);
    }
    const liveFlightsResponse = await this.liveService.getFlights(callsign, aircraft);
    return liveFlightsResponse;
  }
}
