import { Test, TestingModule } from '@nestjs/testing';
import { FlightsService } from './flights.service';
import { flightsProviders } from './flights.providers';
import { DatabaseModule } from '../database/database.module';
import AircraftFilter from './aircraft-filter';

describe('FlightsService', () => {
  let service: FlightsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule],
      providers: [FlightsService, ...flightsProviders],
    }).compile();

    service = module.get<FlightsService>(FlightsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should get LOT aircrafts', async () => {
    const aircrafts = await service.getAircrafts({} as AircraftFilter);
    expect(aircrafts.length).toBeGreaterThan(0);
  });

  it('should get LOT dreamliners', async () => {
    const type = 'B78';
    const aircrafts = await service.getAircrafts({ type: `${type}%` } as AircraftFilter);
    expect(aircrafts.length).toBeGreaterThan(0);
    aircrafts.forEach((aircraft) => {
      expect(aircraft.model).toContain(type);
    });
  });

  it('should get LOT summary', async () => {
    const type = 'B78';
    const aircrafts = await service.getAircraftsSummary(`${type}%`);
    expect(aircrafts.length).toBeGreaterThan(0);
  });
});
