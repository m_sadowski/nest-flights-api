import { Controller, Get, Param, Query } from '@nestjs/common';
import { FlightsService } from './flights.service';
import AircraftFilter from './aircraft-filter';
import { AircraftsSummaryTypePipe } from './pipes/aircrafts-summary-type.pipe';
import { AircraftExt } from './entities/aircraft-ext.entity';
import { Aircraft } from './entities/aircraft.entity';

@Controller('flights')
export class FlightsController {
  constructor(private readonly flightsService: FlightsService) {}

  @Get('/aircrafts')
  async getAircrafts(): Promise<Aircraft[]> {
    return await this.flightsService.getAircrafts({} as AircraftFilter);
  }

  @Get('/summary')
  async getAircraftsSummary(
    @Query('type', new AircraftsSummaryTypePipe()) type,
  ): Promise<AircraftExt[]> {
    return await this.flightsService.getAircraftsSummary(type, 2);
  }

  @Get(':reg')
  async getAircraft(
    @Param() params,
    @Query('lastflights') lastFlightsCount,
  ): Promise<AircraftExt> {
    const { reg } = params;
    lastFlightsCount = parseInt(lastFlightsCount, 0);
    return await this.flightsService.getAircraft(reg, lastFlightsCount);
  }
}
