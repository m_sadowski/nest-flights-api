import { Test, TestingModule } from '@nestjs/testing';
import { ItemsService } from './items.service';
import { Item } from './item.interface';

describe('ItemsService', () => {
  let service: ItemsService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ItemsService],
    }).compile();

    service = module.get<ItemsService>(ItemsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should add items and verify', () => {
    const newItem1 = new Item('Iphone 10', 2599);
    const newItem2 = new Item('Samsung S10', 3999);
    expect(service.create(newItem1)).toEqual(newItem1);
    expect(service.create(newItem2)).toEqual(newItem2);

    const findResult = service.findAll();
    expect(findResult.length).toEqual(2);
    expect(findResult).toContain(newItem1);
    expect(findResult).toContain(newItem2);
  });
});
