export interface AirportFlight {
  flight: string;
  airport: string;
  airline: string;
  aircraft: string;
  aircraftFull: string;
//  from: string;
  to: string;
  time: string;
  timeFull: string;
  status: string;
}

export interface AirportWawFlight {
  flight: string;
  timesch_full: string;
  timesch: string;
  timeexp: string;
  dest: string;
  status: string;
  terminal: string;
  airlines_logo: string;
  airlines: string;
  belt: string;
  exit: string;
  weather: string;
  checkin: string;
  gate: string;
  details: AirportWawFlightDetails;
}

export interface AirportWawFlightDetails {
  aircraft: string;
  aircraft_full: string;
  details_html: string;
}

export interface AirportWawFlightsResponse {
  items: AirportWawFlight[];
  page_quantity: number;
  status: number;
}

export interface AirportWawFlightDetailsResponse {
  readonly details: string;
}

export interface AirportFlightsParams {
  airport?: string;
  airline?: string;
  aircraft?: string;
  page?: number;
  page_count?: number;
  fields?: string[];
}
