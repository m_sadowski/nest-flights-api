import * as dotenv from 'dotenv';
import * as Joi from '@hapi/joi';
import * as fs from 'fs';

export interface EnvConfig {
  [key: string]: string;
}

export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor(filePath: string) {
    const config = dotenv.parse(fs.readFileSync(filePath));
    this.envConfig = this.validateInput(config);
  }

  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid('development', 'production', 'test', 'provision')
        .default(process.env.NODE_ENV || 'development'),
      PORT: Joi.number().default(process.env.PORT || '3000'),
      DB_PORT: Joi.number().default(3306),
      DB_URL: Joi.string().required(),
      DB_NAME: Joi.string().required(),
      DB_USER: Joi.string().required(),
      DB_PASS: Joi.string().required(),
      AUTH0_JWKS_URI: Joi.string().required(),
      AUTH0_AUDIENCE: Joi.string().required(),
      AUTH0_ISSUER: Joi.string().required(),
      AUTH0_ALGORITHM: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } = envVarsSchema.validate(
      envConfig,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }

  get nodeEnv(): string {
    return this.envConfig.NODE_ENV;
  }

  get port(): number {
    return Number(this.envConfig.PORT);
  }

  get dbUrl(): string {
    return this.envConfig.DB_URL;
  }

  get dbPort(): number {
    return Number(this.envConfig.DB_PORT);
  }

  get dbName(): string {
    return this.envConfig.DB_NAME;
  }

  get dbUser(): string {
    return this.envConfig.DB_USER;
  }

  get dbPass(): string {
    return this.envConfig.DB_PASS;
  }

  get auth0JwksUri(): string {
    return this.envConfig.AUTH0_JWKS_URI;
  }

  get auth0Audience(): string {
    return this.envConfig.AUTH0_AUDIENCE;
  }

  get auth0Issuer(): string {
    return this.envConfig.AUTH0_ISSUER;
  }

  get auth0Algorithm(): string {
    return this.envConfig.AUTH0_ALGORITHM;
  }
}
