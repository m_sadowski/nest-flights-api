export class Item {
  public constructor(name?: string, price?: number) {
    if (name) {
      name = name;
    }
    if (price) {
      price = price;
    }
  }

  readonly name: string;
  readonly price: number;
}
