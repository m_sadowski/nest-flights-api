import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import * as path from 'path';
import * as helmet from 'helmet';
import * as winston from 'winston';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import { AppModule } from './app.module';
import { ConfigService } from './config/config.service';
import { Logger } from '@nestjs/common';
import { WinstonModule } from 'nest-winston';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
    {
      logger: WinstonModule.createLogger({
        transports: [
          new winston.transports.Console(),
          new winston.transports.File({ filename: path.join(__dirname, '/logs/error.log'), level: 'error' }),
          new winston.transports.File({ filename: path.join(__dirname, '/logs/app.log') }),
        ],
      }),
    },
  );

  const logger = new Logger('Main');
  const configService = app.get(ConfigService);

  app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));

  app.enableCors();
  app.use(helmet());

  app.useStaticAssets({
    root: join(__dirname, '..', 'public'),
    prefix: '/public/',
  });
  app.setViewEngine({
    engine: {
      handlebars: require('handlebars'),
    },
    templates: join(__dirname, '..', 'views'),
  });

  await app.listen(configService.port, '0.0.0.0');
  logger.log(`App running on port ${configService.port}`);
  logger.log(`Node environment: ${configService.nodeEnv}`);
  logger.log(__dirname);
}
bootstrap();
