import { Module, HttpModule, CacheModule } from '@nestjs/common';
import { AirportsService } from './airports.service';
import { AirportsResolver } from './airports.resolver';
import { AirportsController } from './airports.controller';

@Module({
  imports: [HttpModule, CacheModule.register()],
  providers: [AirportsService, AirportsResolver],
  controllers: [AirportsController],
  exports: [AirportsResolver],
})
export class AirportsModule {}
