export const airportWawTestData = {
  items: [{
    flight: 'LO 3947',
    timesch_full: '20200128154000',
    timesch: '16:40',
    timeexp: '',
    dest: 'Poznań (POZ)',
    status: 'wystartował',
    terminal: 'Strefa  D-E',
    airlines_logo: '<img src="../uploads/airlines_lot_logo_mini.png" alt="" />',
    airlines: 'LOT',
    belt: 'Taśma ',
    exit: 'Wyjście 2',
    weather: '<img src="images/weather/dark/44.png" alt="Częściowe zachmurzenie" /> 7 &deg',
    checkin: '241-280',
    gate: '38',
  }, {
    flight: 'LO 419',
    timesch_full: '20200128160500',
    timesch: '17:05',
    timeexp: '',
    dest: 'Zurych (ZRH)',
    status: 'final call',
    terminal: 'Strefa  D-E',
    airlines_logo: '<img src="../uploads/airlines_lot_logo_mini.png" alt="" />',
    airlines: 'LOT',
    belt: 'Taśma ',
    exit: 'Wyjście 2',
    weather: '<img src="images/weather/dark/44.png" alt="Częściowe zachmurzenie" /> 6 &deg',
    checkin: '241-280',
    gate: '6',
  }],
  page_quantity: 10,
  status: 1,
};

export const airportWawTestDetailsData1 = {
  details: '<b>Nr rejsu:</b><br />LO 3947<br /><br /><b>Linia lotnicza:</b><br />LOT<br /><br /><b>Port docelowy:</b><br />Poznań<br /><br /><b>Samolot:</b><br />DH8D , De Havilland DHC-8 Dash 8-400<br /><br /><b>Check-in:</b><br /> 241-280<br /><br /> <b>Gate:</b><br />38',
};

export const airportWawTestDetailsData2 = {
  details: 'b>Nr rejsu:<\/b><br \/>LO 419<br \/><br \/><b>Linia lotnicza:<\/b><br \/>LOT<br \/><br \/><b>Port docelowy:<\/b><br \/>Zurych<br \/><br \/><b>Samolot:<\/b><br \/>B738 , Boeing 737-800 Passenger (Scimitar winglets)<br \/><br \/><b>Check-in:<\/b><br \/> 241-280<br \/><br \/> <b>Gate:<\/b><br \/>6',
};
