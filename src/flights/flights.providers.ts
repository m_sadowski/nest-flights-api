import { Flight } from './entities/flight.entity';
import { Aircraft } from './entities/aircraft.entity';
import { REPOSITORIES } from './constants';

export const flightsProviders = [
  {
    provide: REPOSITORIES.FLIGHTS_REPOSITORY,
    useValue: Flight,
  },
  {
    provide: REPOSITORIES.AIRCRAFTS_REPOSITORY,
    useValue: Aircraft,
  },
];
