import { Query,  Resolver, Args, Mutation } from '@nestjs/graphql';

@Resolver('Messages')
export class MessagesResolver {
  messagesThatReallyShouldBeInADb = [
    { id: 0, description: 'The seed message' },
    { id: 1, description: 'Message with id 1' },
  ];

  @Query('messages')
  getMessages() {
    return this.messagesThatReallyShouldBeInADb;
  }

  @Query('message')
  getMessage(@Args('id') id: number) {
    return this.messagesThatReallyShouldBeInADb.find((message) => message.id === id);
  }

  @Mutation()
  createMessage(@Args('description') description: string) {
    const id = this.messagesThatReallyShouldBeInADb.length;
    const newMessage = { id, description };
    this.messagesThatReallyShouldBeInADb.push(newMessage);
    return newMessage;
  }
}
