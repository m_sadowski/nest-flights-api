import { ArgumentMetadata, Injectable, PipeTransform, BadRequestException } from '@nestjs/common';

@Injectable()
export class AircraftsSummaryTypePipe implements PipeTransform {
  transform(value: string, metadata: ArgumentMetadata): string {
    const allowedTypes = ['b78', 'b73', 'e7', 'e19', 'a32', 'a34', 'crj7', 'crj9', 'dh8'];
    // if (!value || !allowedTypes.includes(value.toLowerCase())) {
    //   throw new BadRequestException('Validation failed ("type" query parameter)');
    // }
    return value;
  }
}
