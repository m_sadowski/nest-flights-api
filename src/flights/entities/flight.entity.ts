import { Table, Column, Model, DataType } from 'sequelize-typescript';

@Table({timestamps: true, tableName: 'flights'})
export class Flight extends Model<Flight> {

  @Column({ type: DataType.STRING(10), allowNull: false })
  aircraft: string;

  @Column({ type: DataType.STRING(10), allowNull: false })
  callsign: string;

  @Column({ primaryKey: true, autoIncrement: false, type: DataType.STRING(15) })
  id: string;

  @Column({ type: DataType.STRING(10), allowNull: false })
  from: string;

  @Column({ type: DataType.STRING(10), allowNull: false })
  to: string;

  @Column({ type: DataType.STRING(10), allowNull: true })
  reg: string;

  @Column({ type: DataType.DATE, allowNull: true })
  timeSchedDep: Date;

  @Column({ type: DataType.DATE, allowNull: true })
  timeSchedArr: Date;

  @Column({ type: DataType.DATE, allowNull: true })
  timeRealDep: Date;

  @Column({ type: DataType.DATE, allowNull: true })
  timeRealArr: Date;

  @Column({ type: DataType.INTEGER, allowNull: true })
  status: number;

  @Column({ type: DataType.STRING(10), allowNull: true })
  diverted: string;

  @Column({ type: DataType.INTEGER, allowNull: false })
  counter: number;
}
