import { Test, TestingModule } from '@nestjs/testing';
import { AirportsService } from './airports.service';
import { AirportFlightsParams, AirportFlight, AirportWawFlightsResponse } from './airport-flight.interface';
import { HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
import {
  airportWawTestData,
  airportWawTestDetailsData1,
  airportWawTestDetailsData2,
} from './testData/airport.waw.data';

class HttpServiceMock {
  post(url: string, data?: string): Observable<AxiosResponse<AirportWawFlightsResponse>> {

    const axiosResponse: AxiosResponse = {
      data: null,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    if (url.includes('flightboard_get.php')) {
      axiosResponse.data = airportWawTestData;
    } else if (url.includes('flightboard_get_details.php')) {
      if (data.includes(airportWawTestData.items[0].flight)) {
        axiosResponse.data = airportWawTestDetailsData1;
      } else if (data.includes(airportWawTestData.items[1].flight)) {
        axiosResponse.data = airportWawTestDetailsData2;
      }
    }

    const obser = new Observable<AxiosResponse>((subscriber) => {
      subscriber.next(axiosResponse);
      subscriber.complete();
    });

    return obser;
  }
}

describe('AirportsService', () => {
  let service: AirportsService;

  beforeEach(async () => {
    const HttpServiceProvider = {
      provide: HttpService,
      useClass: HttpServiceMock,
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [AirportsService, HttpServiceProvider],
    }).compile();

    service = module.get<AirportsService>(AirportsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return 2 flights from WAW', async () => {
    const result = await service.getFlights({ page: 1, page_count: 1 });
    expect(result.length).toEqual(airportWawTestData.items.length);

    for (let i = 0; i < result.length; i++) {
      const flight = result[i];
      const testData = airportWawTestData.items[i];
      expect(flight.airline).toEqual(testData.airlines);
      expect(flight.flight).toEqual(testData.flight);
      expect(flight.to).toEqual(testData.dest);
      expect(flight.time).toEqual(testData.timesch);
    }
  });
});
