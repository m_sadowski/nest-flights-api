import { Test, TestingModule } from '@nestjs/testing';
import { AirportsResolver } from './airports.resolver';
import { AirportsService } from './airports.service';
import { HttpModule, CacheModule } from '@nestjs/common';
import { AirportFlightsParams, AirportFlight } from './airport-flight.interface';

class AirportsServiceMock {
  getFlights(options: AirportFlightsParams): Promise<AirportFlight[]> {
    return Promise.resolve([
      null,
    ]);
  }
}

describe('AirportsResolver', () => {
  let resolver: AirportsResolver;

  beforeEach(async () => {

    const AirportsServiceProvider = {
      provide: AirportsService,
      useClass: AirportsServiceMock,
    };

    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule, CacheModule.register()],
      providers: [AirportsServiceProvider, AirportsResolver],
    }).compile();

    resolver = module.get<AirportsResolver>(AirportsResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
