import { Resolver, Query, Args, Info } from '@nestjs/graphql';
import { Inject } from '@nestjs/common';
import { AirportsService } from './airports.service';
import { AirportFlightsParams } from './airport-flight.interface';

@Resolver('Airports')
export class AirportsResolver {

  constructor(
    @Inject(AirportsService) private readonly airportsService: AirportsService,
  ) {}

  @Query('airportFlights')
  async getFlights(
    @Args('airport') airport: string,
    @Args('aircraft') aircraft: string,
    @Args('page') page: number,
    // tslint:disable-next-line: variable-name
    @Args('page_count') page_count: number,
    @Info() info,
  ) {

    // extract requested fields from info
    const requestedFields = info.fieldNodes[0].selectionSet.selections.map((selection => selection.name.value));

    const airportFlightsParams: AirportFlightsParams = { airport, aircraft, page, page_count, fields: requestedFields };
    const airportFlights = await this.airportsService.getFlights(airportFlightsParams);
    return airportFlights;
  }
}
