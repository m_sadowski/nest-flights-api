import { NestMiddleware, Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import * as jwt from 'express-jwt';
import { expressJwtSecret } from 'jwks-rsa';
import { ConfigService } from 'src/config/config.service';

@Injectable()
export class AuthenticationMiddleware implements NestMiddleware {

  constructor(private readonly configService: ConfigService) {}

  use(req, res, next) {
    jwt({
      secret: expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: this.configService.auth0JwksUri,
      }),
      audience: this.configService.auth0Audience,
      issuer: this.configService.auth0Issuer,
      algorithm: this.configService.auth0Algorithm,
    })(req, res, err => {
      // console.log('user');
      // console.log(req);
      if (err) {
        const status = err.status || 500;
        const message =
          err.message || 'Sorry, we were unable to process your request.';

        console.log(message);
        // console.log(res);
        // return res.status(status).send({
        //   message,
        // });
        /*
        return res.send({
          message,
        });
        */
        // return next(new Error(message));
        throw new UnauthorizedException(null, message);
      }
      next();
    });
  }
}
