#!/bin/bash

LOCAL_DIR="/home/mariusz/Documents/Dev/nest-flights-api"
REMOTE_DIR=~"/domains/stage.fl.msdev.ovh/public_nodejs"
SSH_ADDRESS="msdev@s25.mydevil.net"

rsync -v -e "ssh" $LOCAL_DIR/src/schema.graphql $SSH_ADDRESS:$REMOTE_DIR
rsync -v -e "ssh" $LOCAL_DIR/package.json $SSH_ADDRESS:$REMOTE_DIR
rsync -v -e "ssh" $LOCAL_DIR/stage.env $SSH_ADDRESS:$REMOTE_DIR
rsync -v -r -e "ssh" $LOCAL_DIR/dist/* $SSH_ADDRESS:$REMOTE_DIR
rsync -v -r -e "ssh" $LOCAL_DIR/views $SSH_ADDRESS:$REMOTE_DIR