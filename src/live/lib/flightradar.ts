import * as request from 'request';

interface FlightsQueryParams {
  callsign: string;
  aircraft?: string;
}

interface FlightNumber {
  default: string;
  alternative?: string; // nullable
}

interface FlightIdentification {
  id: string;
  row: number;
  number: FlightNumber;
  callsign: string;
}

interface FlightStatusGenericStatus {
  text: string; // np estimated, landed
  color: string; // red, yellow, green
  diverted?: string; // gdzie przekierowany (opcjonalne property)
  type: string; // np arrival
}

interface FlightStatusEventTime {
  utc: number;
  local: number;
}

interface FlightStatusGeneric {
  status: FlightStatusGenericStatus;
  eventTime?: FlightStatusEventTime;
}

interface FlightStatus {
  live: boolean;
  text: string;
  icon: string;
  // estimated?:
  ambiguous: boolean;
  generic: FlightStatusGeneric;
}

interface AircraftModel {
  code: string;
  text: string;
}

interface Aircraft {
  model: AircraftModel;
  countryId: number;
  registration: string;
  images: object;
}

interface AirlineCode {
  iata: string;
  icao: string;
}

interface Airline {
  name: string;
  short: string;
  code: AirlineCode;
  url: string;
}

interface AirportCode {
  iata: string;
  icao: string;
}

interface AirportCountry {
  name: string;
  code: string;
  codeLong: string;
}

interface AirportRegion {
  city: string;
}

interface AirportPosition {
  latitude: number;
  longitude: number;
  altitude: number;
  country: AirportCountry;
  region: AirportRegion;
}

interface AirportTimezone {
  name: string;
  offset: number;
  offsetHours: string;
  abbr: string;
  abbrName: string;
  isDst: boolean;
}

interface AirportData {
  name: string;
  code: AirportCode;
  position: AirportPosition;
  timezone: AirportTimezone;
  visible: boolean;
  website: string;
}

interface Airport {
  origin: AirportData;
  destination: AirportData;
  real?: AirportData; // w przypadku diverta dodatkowe airport real
}

interface FlightTimeInfo {
  departure?: number;
  arrival?: number;
}

interface FlightTimeOther {
  eta?: number;
  updated?: number;
}

interface FlightTimeHistorical {
  flighttime?: string;
  delay?: string;
}

interface FlightTime {
  scheduled: FlightTimeInfo;
  real: FlightTimeInfo;
  estimated: FlightTimeInfo;
  other: FlightTimeOther;
  historical: FlightTimeHistorical;
}

interface FlightDetails {
  identification: FlightIdentification;
  status: FlightStatus;
  aircraft: Aircraft;
  airline: Airline;
  airport: Airport;
  time: FlightTime;
}

interface Flight {
  id: string;
  callsign: string;
  from: string;
  to: string;
  aircraft: string;
  reg: string;
  details: FlightDetails;
}

interface GetDetailsAllResult {
  errors: number;
  flights: Flight[];
}

class FlightradarService {
  public getStatusNumber(statusColor: string): number {
    let status = 0;
    switch (statusColor) {
      case 'green':
        status = 1;
        break;
      case 'yellow':
        status = 2;
        break;
      case 'red':
        status = 3;
        break;
      default:
        status = 0;
    }
    return status;
  }

  public async getAll(
    queryParams: FlightsQueryParams,
    func?: (fl: any) => any,
  ): Promise<Flight[]> {
    const zonesData = await this.getFrZonesData(
      this.calculateZones(90, 90),
      queryParams,
    );
    return this.getFlightsList(zonesData, func);
  }

  public getFlightDetails(flightId: string): Promise<FlightDetails> {
    return new Promise(async (resolve, reject) => {
      const detailsUrl: string = `https://data-live.flightradar24.com/clickhandler/?version=1.5&flight=${flightId}`;
      request(
        detailsUrl,
        { json: true },
        (err: Error, res: request.Response, body: any) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(body as FlightDetails);
        },
      );
    });
  }

  /**
   *
   * @param flights
   */
  public getFlightDetailsAll(flights: Flight[]): Promise<GetDetailsAllResult> {
    return new Promise(async (resolve, reject) => {
      let counter = 0;
      let errors = 0;
      for (const flight of flights) {
        try {
          const details: any = await this.getFlightDetails(flight.id);
          counter++;
          if (this.isObject(details)) {
            delete details.flightHistory;
            delete details.trail;
            flight.details = details;
          } else {
            errors++;
          }
          if (counter >= flights.length) {
            resolve({ errors, flights } as GetDetailsAllResult);
          }
        } catch (err) {
          reject(err);
        }
      }
    });
  }

  /**
   * Gets flight details with retrying on errors
   * @param flights
   */
  public getFlightDetailsAllWithRetries(flights: Flight[]): Promise<Flight[]> {
    return new Promise(async (resolve, reject) => {
      const retriesCount: number = 15;
      const retriesSleep: number = 2000;
      let flightsToGet: Flight[] = flights;

      for (let i = 1; i <= retriesCount; i++) {
        try {
          const getAllResult = await this.getFlightDetailsAll(flightsToGet);
          if (getAllResult.errors > 0) {
            await this.sleep(retriesSleep);
            flightsToGet = getAllResult.flights.filter(
              (flight: any) => flight.details === undefined,
            );
          } else {
            resolve(flights as Flight[]);
            break;
          }
          if (i === retriesCount) {
            reject(new Error('Too many errors on getting details.'));
          }
        } catch (err) {
          // TODO logowanie bledu pobierania details
          // console.log(err);
        }
      }
    });
  }

  /**
   * Calculates zones
   * @param offsetX
   * @param offsetY
   */
  private calculateZones(offsetX: number, offsetY: number): number[][] {
    const zones: number[][] = [];
    for (let y: number = -90; y <= 90 - offsetY; y += offsetY) {
      for (let x: number = -180; x <= 180 - offsetX; x += offsetX) {
        zones.push([y + offsetY, y, x, x + offsetX]);
      }
    }
    return zones;
  }

  /**
   * Returns flights list as object. Key is flight id, value is array of data to parse
   * @param zone Array of square coordinates
   * @param queryParams
   */
  private getFrZoneData(
    zone: number[],
    queryParams: FlightsQueryParams,
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let zoneUrl: string = `https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=${zone[0]},${zone[1]},${zone[2]},${zone[3]}&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1`;

      if (queryParams.callsign) {
        zoneUrl += `&callsign=${queryParams.callsign.toUpperCase()}`;
      } else {
        reject(new Error('GetFrZoneData - callsign not provided.'));
      }
      if (queryParams.aircraft) {
        zoneUrl += `&type=${queryParams.aircraft.toUpperCase()}`;
      }
      request(
        zoneUrl,
        { json: true },
        (err: Error, res: request.Response, body: any) => {
          if (err) {
            reject(err);
            return;
          }
          delete body.full_count;
          delete body.version;
          delete body.stats;
          delete body['selected-aircraft'];

          resolve(body);
        },
      );
    });
  }

  /**
   * Returns Object. Key is flight id, value is flight data as array
   * @param zonesArr
   * @param queryParams
   */
  private getFrZonesData(
    zonesArr: number[][],
    queryParams: FlightsQueryParams,
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const frZonePromises: any[] = [];
      zonesArr.forEach(zone => {
        frZonePromises.push(this.getFrZoneData(zone, queryParams));
      });
      const allZonesDataArr: any[] = await Promise.all(frZonePromises);
      const allZonesData = Object.assign({}, ...allZonesDataArr);
      resolve(allZonesData);
    });
  }

  /**
   * Returns flights array
   * @param zoneData Object. Key is flight id, value is flight data as array
   * @param func Callback function to execute on each data record
   */
  private getFlightsList(zoneData: any, func?: (fl: any) => any): Flight[] {
    const flights: Flight[] = [];
    Object.keys(zoneData).forEach((key: string) => {
      const val: any[] = zoneData[key];
      if (Array.isArray(val) && val.length >= 14) {
        let flight = {
          aircraft: val[8],
          callsign: val[13],
          from: val[11],
          id: key,
          reg: val[9],
          to: val[12],
        };
        if (func) {
          flight = func(flight);
        }
        flights.push(flight as Flight);
      }
    });
    return flights;
  }

  // helpers
  private isObject(obj: any): boolean {
    return (
      (typeof obj === 'object' && obj !== null) || typeof obj === 'function'
    );
  }

  private sleep(milliseconds: number): Promise<any> {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  }
}
export default FlightradarService;
export { Flight, AirportData, FlightsQueryParams };
