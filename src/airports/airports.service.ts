import { Injectable, HttpService } from '@nestjs/common';
import { AirportFlight, AirportWawFlightsResponse, AirportWawFlightDetailsResponse, AirportFlightsParams } from './airport-flight.interface';

@Injectable()
export class AirportsService {

  private readonly AIRPORT_WAW_URL = 'https://www.lotnisko-chopina.pl/includes';

  constructor(private readonly httpService: HttpService) {}

  private checkWawGetDetails(fields, aircraft) {
    fields = fields || [];
    if (
      fields.length === 0 ||
      fields.includes('aircraft') ||
      fields.includes('aircraftFull')
    ) {
      return true;
    } else if (aircraft) {
      return true;
    }
    return false;
  }

  public async getWawFlights(page = 1, airline = 'LOT'): Promise<AirportWawFlightsResponse> {

    return new Promise(async (resolve, reject) => {
      const postData = `phrase=&type=D&time=0&port=0&airlines=${airline}&lang=en&page_number=${page}`;
      const flightsResponse = this.httpService.post<AirportWawFlightsResponse>(
        `${this.AIRPORT_WAW_URL}/flightboard_get.php`,
        postData,
      ).toPromise();
      try {
        const flights = await flightsResponse;
        resolve(flights.data);
      } catch (err) {
        reject(err);
      }
    });
  }

  public async getWawFlightsWithDetails(page = 1, airline = 'LOT', aircraft = '', fields = []): Promise<AirportWawFlightsResponse> {

    return new Promise(async (resolve, reject) => {
      const flightsDetailsPromises = [];
      const flights = await this.getWawFlights(page, airline);
      if (this.checkWawGetDetails(fields, aircraft)) {
        // console.log('getdetails');
        // console.log(fields);
        flights.items.forEach(async item => {
          const postDetailsData = `flight=${item.flight}&timesch=${item.timesch_full}&type=D&lang=en`;
          const flightDetailsResponse = this.httpService.post<AirportWawFlightDetailsResponse>(
            `${this.AIRPORT_WAW_URL}/flightboard_get_details.php`,
            postDetailsData,
          ).toPromise();
          flightsDetailsPromises.push(flightDetailsResponse);
          try {
            const flightDetails = await flightDetailsResponse;
            const flightDetailsHtml = flightDetails.data.details;
            const flightDetailsSplitted = flightDetailsHtml.split('<br />');
            const aircraftSplitted = flightDetailsSplitted[10].split(' , ');
            item.details = { details_html: flightDetailsHtml, aircraft: aircraftSplitted[0], aircraft_full: aircraftSplitted[1] };
          // tslint:disable-next-line: no-empty
          } catch (err) {}
        });
        await Promise.all(flightsDetailsPromises);

        if (aircraft) {
          flights.items = flights.items.filter(fl => fl.details.aircraft.toLowerCase().includes(aircraft.toLowerCase()));
        }
      }
      resolve(flights);
    });
  }

  public async getFlights(options: AirportFlightsParams): Promise<AirportFlight[]> {

    return new Promise(async (resolve, reject) => {
      const flightsPromises = [];
      for (let i = 0; i < options.page_count; i++) {
        flightsPromises.push(this.getWawFlightsWithDetails(i + options.page, options.airline, options.aircraft, options.fields));
      }
      const flights = await Promise.all(flightsPromises);
      const result: AirportFlight[] = [];
      flights.forEach((f) => {
        f.items.forEach((fl) => {
          result.push({
            aircraft: fl.details ? fl.details.aircraft : '',
            aircraftFull: fl.details ? fl.details.aircraft_full : '',
            airline: fl.airlines,
            airport: 'Warsaw (WAW)',
            flight: fl.flight,
            time: fl.timesch,
            timeFull: fl.timesch_full,
            to: fl.dest,
            status: fl.status,
          });
        });
      });
      resolve(result);
    });
  }
}
