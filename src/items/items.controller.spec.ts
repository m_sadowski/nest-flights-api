import { Test, TestingModule } from '@nestjs/testing';
import { ItemsController } from './items.controller';
import { ItemsService } from './items.service';
import { Item } from './item.interface';
import { CreateItemDto } from './create-item.dto';

class ItemsServiceMock {
  private readonly items: Item[] = [];

  findAll(): Item[] {
    return this.items;
  }

  create(item: Item): Item {
    this.items.push(item);
    return item;
  }
}

describe('Items Controller', () => {
  let controller: ItemsController;

  beforeEach(async () => {
    const ItemsServiceProvider = {
      provide: ItemsService,
      useClass: ItemsServiceMock,
    };
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ItemsController],
      providers: [ItemsServiceProvider],
    }).compile();

    controller = module.get<ItemsController>(ItemsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  // it('should throw error on not valid data', () => {
  //   // const newItem = new CreateItemDto('philips', 2233);
  //   const newItem = { name: 'philips tv', price: '2233' };
  //   expect(controller.create(null)).toThrowError();
  // });

  it('should create and check item', async () => {
    const newItem = new CreateItemDto('iphone 10', 2299);
    const createResult = await controller.create(newItem);
    expect(createResult.name).toEqual(newItem.name);
    expect(createResult.price).toEqual(newItem.price);

    const findAllResult = await controller.findAll();
    expect(findAllResult.length).toEqual(1);
    // expect(findAllResult[0].name).toEqual(newItem.name);
    // expect(findAllResult[0].price).toEqual(newItem.price);
    // checks all properties recursively, not reference
    expect(findAllResult[0]).toEqual(newItem);
  });

  it('shlould test mock function', () => {

    function forEach(items, callback) {
      // tslint:disable-next-line: prefer-for-of
      for (let index = 0; index < items.length; index++) {
        callback(items[index]);
      }
    }

    const mockCallback = jest.fn(x => 42 + x);
    forEach([0, 1], mockCallback);

    // The mock function is called twice
    expect(mockCallback.mock.calls.length).toBe(2);

    // The first argument of the first call to the function was 0
    expect(mockCallback.mock.calls[0][0]).toBe(0);

    // The first argument of the second call to the function was 1
    expect(mockCallback.mock.calls[1][0]).toBe(1);

    // The return value of the first call to the function was 42
    expect(mockCallback.mock.results[0].value).toBe(42);

    // The return value of the second call to the function was 43
    expect(mockCallback.mock.results[1].value).toBe(43);
  });

});
