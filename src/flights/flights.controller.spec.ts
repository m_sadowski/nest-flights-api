import { Test, TestingModule } from '@nestjs/testing';
import { FlightsController } from './flights.controller';
import { FlightsService } from './flights.service';
import { DatabaseModule } from '../database/database.module';
import { flightsProviders } from './flights.providers';

describe('Flights Controller', () => {
  let controller: FlightsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule],
      controllers: [FlightsController],
      providers: [FlightsService, ...flightsProviders],
    }).compile();

    controller = module.get<FlightsController>(FlightsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should get LOT aircraft SP-LSC', async () => {
    const reg = 'sp-lsc';
    const aircraft = await controller.getAircraft({ reg }, 3);
    expect(aircraft).toBeDefined();
    expect(aircraft.reg.toLowerCase()).toBe(reg);
  });
});
