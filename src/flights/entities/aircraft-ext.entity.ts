import { Aircraft } from './aircraft.entity';
import { Flight } from './flight.entity';

export class AircraftExt extends Aircraft {
  flights?: Flight[];
}
