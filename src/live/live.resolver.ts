import { Resolver, Query, Args, Info } from '@nestjs/graphql';
import FlightRadarService, { Flight, FlightsQueryParams } from './lib/flightradar';

@Resolver('LiveFlights')
export class LiveResolver {

  @Query('liveFlights')
  async getFlights(
    @Info() info,
    @Args('callsign') callsign: string,
    @Args('aircraft') aircraft: string,
  ) {

    // details pobierac tylko wtedy, gdy jest request po szczegoly
    // console.log(info);

    const flightsParams: FlightsQueryParams = { callsign };

    if (aircraft) {
      flightsParams.aircraft = aircraft;
    }

    const frService = new FlightRadarService();
    const flights: Flight[] = await frService.getAll(flightsParams);
    return flights;
  }
}
