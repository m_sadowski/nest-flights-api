import { Injectable, Inject } from '@nestjs/common';
import { Op } from 'sequelize';
import { REPOSITORIES } from './constants';
import { Flight } from './entities/flight.entity';
import { Aircraft } from './entities/aircraft.entity';
import { AircraftExt } from './entities/aircraft-ext.entity';
import AircraftFilter from './aircraft-filter';

@Injectable()
export class FlightsService {
  constructor(
    @Inject(REPOSITORIES.FLIGHTS_REPOSITORY) private readonly flightsRepository: typeof Flight,
    @Inject(REPOSITORIES.AIRCRAFTS_REPOSITORY) private readonly aircraftsRepository: typeof Aircraft,
  ) {}

  async findAll(): Promise<Flight[]> {
    return this.flightsRepository.findAll<Flight>();
  }

  async getAircrafts(filter: AircraftFilter): Promise<Aircraft[]> {

    const dbWhere: any = {};

    if (filter.reg) {
      // Can provide regs separated by ','
      const regs = filter.reg.split(',');
      if (regs.length > 0) {
        const regsQuery = regs.map((reg) => {
          return { [Op.like]: reg };
        });
        dbWhere.reg = { reg: { [Op.or]: regsQuery }};
      }
    }

    if (filter.type) {
      dbWhere.model = { [Op.like]: filter.type };
    }

    const dbQuery = { where: dbWhere };
    const aircrafts: Aircraft[] = await this.aircraftsRepository.findAll<Aircraft>(dbQuery);
    const result: Aircraft[] = aircrafts.map(st => st.get({ plain: true }) as Aircraft);

    return result;
  }

  private async getAircraftLastFlights(reg, count): Promise<Flight[]> {

    let flights: Flight[] = await this.flightsRepository.findAll<Flight>({
      where: {
        reg,
      },
      order: [[ 'createdAt', 'DESC' ]],
      limit: count,
    });
    flights = flights.map(st => st.get({ plain: true }) as Flight);
    return flights;
  }

  async getAircraftsSummary(type: string, lastFlightsCount: number = 3): Promise<AircraftExt[]> {

    const aircrafts: Aircraft[] = (await this.getAircrafts({ type } as AircraftFilter));
    const flightsArr: Flight[][] = await Promise.all(aircrafts.map(aircraft => this.getAircraftLastFlights(aircraft.reg, lastFlightsCount)));
    const result: AircraftExt[] = new Array<AircraftExt>();

    for (let i = 0; i < aircrafts.length; i++) {
      const aircraftExt: AircraftExt = aircrafts[i] as AircraftExt;
      aircraftExt.flights = flightsArr[i];
      result.push(aircraftExt);
    }

    return result;
  }

  async getAircraft(reg, lastFlightsCount): Promise<AircraftExt> {

    const aircraft: Aircraft = await this.aircraftsRepository.findOne<Aircraft>({
      where: {
        reg: { [Op.like]: reg },
      },
    });
    const result: AircraftExt = aircraft.get({ plain: true }) as AircraftExt;
    if (lastFlightsCount > 0) {
      result.flights = await this.getAircraftLastFlights(reg, lastFlightsCount);
    }
    return result;
  }
}
