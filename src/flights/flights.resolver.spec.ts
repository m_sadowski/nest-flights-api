import { Test, TestingModule } from '@nestjs/testing';
import { FlightsResolver } from './flights.resolver';
import { Flight } from './entities/flight.entity';
import { FlightsService } from './flights.service';
import { flightsProviders } from './flights.providers';

// class FlightsServiceMock {
//   findAll(): Promise<Flight[]> {
//     return Promise.resolve([
//       null,
//     ]);
//   }
// }

describe('FlightsResolver', () => {
  let resolver: FlightsResolver;

  beforeEach(async () => {

    // const FlightsRepository = {
    //   provide: 'FLIGHTS_REPOSITORY',
    //   useClass: FlightsServiceMock,
    // };

    const module: TestingModule = await Test.createTestingModule({
      providers: [...flightsProviders, FlightsResolver],
    }).compile();

    resolver = module.get<FlightsResolver>(FlightsResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
