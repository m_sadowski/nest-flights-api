## Description

Flights API. Project developed to learn Nest JS & Typescript.

Created in [Nest](https://github.com/nestjs/nest) framework.

## Url

https://stage.fl.msdev.ovh/

## Main API endpoints
* /live to get current flights from Flightradar24 service
* /airports to get departures from selected airport (Warsaw only)
* /flights to get flights history of LOT Polish Airlines

## GraphQL

Application provides GraphQL endpoint

https://stage.fl.msdev.ovh/graphql

```javascript
// Current LOT flights
query {
  liveFlights(callsign:"LOT") {
    callsign
    from
    to
    aircraft
  }
}

// Departures from Warsaw Chopin Airport
query {
  airportFlights(airport:"WAW", page:1, page_count:1) {
    flight
    to
    time
    status
  }
}

// Flights history (LOT Polish Airlines)
query {
  flights(page:1, pageSize:10) {
    callsign
    from
    to
    timeSchedDep
    timeSchedArr
    aircraft
    reg
  }
}

```

## Data source

Live flights & airport flights are fetched directly from Flightradar24 & Warsaw airport page.
Flights history of LOT Polish Airlines is stored in MySQL database.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Configuration

Copy example.env content to file named #NODE_ENV#.env. i.e. development.env, production.env
