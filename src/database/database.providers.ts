import { Sequelize } from 'sequelize-typescript';
import { ConfigService } from '../config/config.service';
import { PROVIDERS } from './constants';
import { Flight } from '../flights/entities/flight.entity';
import { Aircraft } from '../flights/entities/aircraft.entity';
import { Airport } from '../flights/entities/airport.entity';

export const databaseProviders = [
  {
    provide: PROVIDERS.SEQUELIZE,
    useFactory: async (config: ConfigService) => {
      const sequelize = new Sequelize({
        dialect: 'mysql',
        host: config.dbUrl,
        port: config.dbPort,
        username: config.dbUser,
        password: config.dbPass,
        database: config.dbName,
        logging: false,
      });
      sequelize.addModels([Flight, Aircraft, Airport]);
      try {
        await sequelize.sync();
      // tslint:disable-next-line: no-empty
      } catch (err) {}
      return sequelize;
    },
    inject: [ConfigService],
  },
];
