import { AirportFlightsParams, AirportFlight } from '../airport-flight.interface';

export class AirportBase {
  constructor(protected readonly code: string, protected readonly name: string) {}

  getFlights(options: AirportFlightsParams): Promise<AirportFlight[]> {
    throw new Error('Not implemented');
  }
}
