import { Table, Column, Model, DataType } from 'sequelize-typescript';

@Table({timestamps: true, tableName: 'airports'})
export class Airport extends Model<Airport> {

  @Column({ primaryKey: true, autoIncrement: false, type: DataType.STRING(10) })
  iata: string;

  @Column({ type: DataType.STRING(10), allowNull: false })
  icao: string;

  @Column({ type: DataType.STRING(50), allowNull: false })
  country: string;

  @Column({ type: DataType.STRING(50), allowNull: true })
  city: string;

  @Column({ type: DataType.FLOAT, allowNull: false })
  latitude: string;

  @Column({ type: DataType.FLOAT, allowNull: false })
  longitude: string;

  @Column({ type: DataType.FLOAT, allowNull: true })
  altitude: string;

  @Column({ type: DataType.STRING(50), allowNull: false })
  timezone: string;

  @Column({ type: DataType.INTEGER, allowNull: false })
  timezoneOffset: string;

  @Column({ type: DataType.STRING(256), allowNull: true })
  website: string;
}
