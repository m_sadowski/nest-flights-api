import { HttpModule, CacheModule, HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AirportsController } from './airports.controller';
import { AirportsService } from './airports.service';
import { AirportFlightsParams, AirportFlight } from './airport-flight.interface';
import { expressJwtSecret } from 'jwks-rsa';

const airportFlights = [
  {
    aircraft: 'E75S',
    aircraftFull: 'Embraer 175 S (Short wing)',
    airline: 'LOT',
    airport: 'Warsaw (WAW)',
    flight: 'LO 641',
    time: '11:25',
    timeFull: '20200309102500',
    to: 'Bucharest (OTP)',
    status: 'departed',
  } as AirportFlight,
  {
    aircraft: 'B789',
    aircraftFull: 'Boeing 787-900',
    airline: 'LOT',
    airport: 'Warsaw (WAW)',
    flight: 'LO 079',
    time: '14:40',
    timeFull: '20200309134000',
    to: 'Tokyo-Narita (NRT)',
    status: '',
  } as AirportFlight,
];

class AirportsServiceMock {
  getFlights(options: AirportFlightsParams): Promise<AirportFlight[]> {
    return Promise.resolve(airportFlights);
  }
}

describe('Airports Controller', () => {
  let controller: AirportsController;

  beforeEach(async () => {
    const AirportsServiceProvider = {
      provide: AirportsService,
      useClass: AirportsServiceMock,
    };
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule, CacheModule.register()],
      controllers: [AirportsController],
      providers: [AirportsServiceProvider],
    }).compile();

    controller = module.get<AirportsController>(AirportsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return mock flights', async () => {

    const result = await controller.getFlights('WAW', 'lot', 1, 1);
    expect(result.length).toEqual(airportFlights.length);

    for(let i = 0; i < result.length; i++) {
      const fl = result[i];
      const pattern = airportFlights[i];
      for(let key in fl) {
        expect(fl[key]).toEqual(pattern[key]);
      }
    }
  });
});
