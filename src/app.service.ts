import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  // getHello(): string {
  //   return 'MSDEV Flights API';
  // }
  getIndex(): any {
    return {
      title: 'MSDEV Flights API',
      intro: 'Welcome onboard ! Use one of endpoints listed below:',
      endpoints: [
        '/live?callsign=lot > live flights',
        '/airports?airport=waw > departures from Warsaw airport',
        '/flights/sp-lsg?lastflights=3 > SP-LSG aircraft info and 3 last flights',
      ],
    };
  }
}
