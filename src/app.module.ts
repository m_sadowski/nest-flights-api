import { Module, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';
import * as path from 'path';
import { AuthenticationMiddleware } from './common/authentication.middleware';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ItemsController } from './items/items.controller';
import { ItemsService } from './items/items.service';
import { MessagesModule } from './messages/messages.module';
import { FlightsModule } from './flights/flights.module';
import { DatabaseModule } from './database/database.module';
import { ConfigModule } from './config/config.module';
import { LiveModule } from './live/live.module';
import { AirportsModule } from './airports/airports.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      resolverValidationOptions: {
        requireResolversForResolveType: false,
      },
    }),
    WinstonModule.forRoot({
      transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: path.join(__dirname, '/logs/error.log'), level: 'error' }),
        new winston.transports.File({ filename: path.join(__dirname, '/logs/app.log') }),
      ],
    }),
    MessagesModule,
    FlightsModule,
    DatabaseModule,
    ConfigModule,
    LiveModule,
    AirportsModule,
    AuthModule,
    UsersModule,
  ],
  controllers: [AppController, ItemsController],
  providers: [AppService, ItemsService, AuthenticationMiddleware],
})
export class AppModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthenticationMiddleware)
      .forRoutes(
        { path: '/items', method: RequestMethod.POST },
        { path: '/shopping-cart', method: RequestMethod.POST },
      );
  }
}
