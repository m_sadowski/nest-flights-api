import { Test, TestingModule } from '@nestjs/testing';
import { LiveController } from './live.controller';
import { Flight } from './lib/flightradar';
import { LiveService } from './live.service';

const liveFlights = [
  {
    callsign: 'LO79',
    aircraft: 'B788',
    reg: 'SP-LRH',
    to: 'NRT',
  } as Flight,
];

class LiveServiceMock {
  async getFlights(callsign: string, aircraft: string): Promise<Flight[]> {
    return Promise.resolve([
      null,
    ]);
  }
}

describe('Live Controller', () => {
  let controller: LiveController;

  beforeEach(async () => {
    const LiveServiceProvider = {
      provide: LiveService,
      useClass: LiveServiceMock,
    };
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LiveController],
      providers: [LiveServiceProvider],
    }).compile();

    controller = module.get<LiveController>(LiveController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return mock flights', async () => {
    const flights = await controller.getLiveFlights('lot', null);
    expect(flights.length).toBeGreaterThan(0);
  });
});
