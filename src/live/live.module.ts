import { Module } from '@nestjs/common';
import { LiveService } from './live.service';
import { LiveController } from './live.controller';
import { LiveResolver } from './live.resolver';

@Module({
  providers: [LiveService, LiveResolver],
  controllers: [LiveController],
  exports: [LiveResolver],
})
export class LiveModule {}
