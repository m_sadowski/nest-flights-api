import { Controller, Get, Query, UseInterceptors, CacheInterceptor, CacheKey, CacheTTL, HttpException } from '@nestjs/common';
import { LoggingInterceptor } from '../common/logging.interceptor';
import { AirportsService } from './airports.service';
import { AirportFlight } from './airport-flight.interface';

@Controller('airports')
@UseInterceptors(CacheInterceptor, LoggingInterceptor)
export class AirportsController {
  constructor(private readonly airportsService: AirportsService) {}

  // @CacheKey('airport_flights')
  @Get()
  @CacheTTL(300)
  async getFlights(
    @Query('airport') airport,
    @Query('airline') airline,
    @Query('page') page,
    @Query('pagecount') pagecount,
  ): Promise<AirportFlight[]> {
    if (!airport) {
      throw new HttpException('Incomplete query', 400);
    }
    const flightsResponse = await this.airportsService.getFlights({ airport, airline, page: page || 1, page_count: pagecount || 1 });
    return flightsResponse;
  }
}
