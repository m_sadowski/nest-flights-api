import { AircraftsSummaryTypePipe } from './aircrafts-summary-type.pipe';

describe('AircraftsSummaryTypePipe', () => {
  it('should be defined', () => {
    expect(new AircraftsSummaryTypePipe()).toBeDefined();
  });
});
