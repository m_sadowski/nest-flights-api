import { Injectable } from '@nestjs/common';
import FlightRadarService, { Flight, FlightsQueryParams } from './lib/flightradar';

@Injectable()
export class LiveService {

  async getFlights(
    callsign: string,
    aircraft: string,
  ) {
    const flightsParams: FlightsQueryParams = { callsign };

    if (aircraft) {
      flightsParams.aircraft = aircraft;
    }

    const frService = new FlightRadarService();
    const flights: Flight[] = await frService.getAll(flightsParams);
    return flights;
  }
}
