import { Query,  Resolver, Args } from '@nestjs/graphql';
import { Inject } from '@nestjs/common';
import { Op } from 'sequelize';
import { Flight } from './entities/flight.entity';

@Resolver('Flights')
export class FlightsResolver {

  constructor(
    @Inject('FLIGHTS_REPOSITORY') private readonly flightsRepository: typeof Flight,
  ) {}

  @Query('flights')
  async getFlights(
    @Args('callsign') callsign: string,
    @Args('reg') reg: string,
    @Args('aircraft') aircraft: string,
    @Args('from') from: string,
    @Args('to') to: string,
    @Args('page') page: number,
    @Args('pageSize') pageSize: number,
  ) {
    const offset = page * pageSize;
    const limit = pageSize;
    const where: any = { };

    if (callsign) { where.callsign = { [Op.like]: callsign }; }
    if (reg) { where.reg = { [Op.like]: reg }; }
    if (aircraft) { where.aircraft = { [Op.like]: aircraft }; }
    if (from) { where.from = { [Op.like]: from }; }
    if (to) { where.to = { [Op.like]: to }; }

    let flights = await this.flightsRepository.findAll<Flight>({
      where,
      order: [[ 'createdAt', 'DESC' ]],
      limit,
      offset,
    });
    flights = flights.map(fl => fl.get({ plain: true }));
    return flights;
  }
}
