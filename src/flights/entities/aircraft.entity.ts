import { Table, Column, Model, DataType } from 'sequelize-typescript';

@Table({timestamps: true, tableName: 'aircraft'})
export class Aircraft extends Model<Aircraft> {

  @Column({ primaryKey: true, autoIncrement: false, type: DataType.STRING(10) })
  reg: string;

  @Column({ type: DataType.STRING(10), allowNull: false })
  model: string;

  @Column({ type: DataType.STRING(100), allowNull: false })
  full: string;

  @Column({ type: DataType.STRING(4096), allowNull: true })
  images: string;
}
